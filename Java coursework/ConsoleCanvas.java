/**
 * 
 */
package droneSim;

/**
 * @author Axel
 *
 */
public class ConsoleCanvas {
	
	private char[][] Arena;
	private int w,h;
	/**
	 * constructor
	 * @param x
	 * @param y
	 */
	public ConsoleCanvas(int x, int y) {
		w = x;
		h = y;
		Arena = new char[x][y];
		makeTable();
	}
	/**
	 * three different for loops:
	 * two create the boundaries of the arena
	 * the 3rd fills in the arena with whitespace
	 */
	private void makeTable() {
		for(int i = 0; i < w; i++) {
			Arena[i][0] = '#';
			Arena[i][h-1] = '#';
		}
		for (int j = 0; j < h; j++) {
			Arena[0][j] = '#';
			Arena[w-1][j] = '#';
		}
		for(int n = 1; n < w-1; n++) {
			for(int m = 1; m < h-1; m++) {
				Arena[n][m] = ' ';
			}
		}
	}
	/**
	 * places a char on a specified location to represent a drone 
	 * @param x
	 * @param y
	 * @param r
	 */
	public void showIt(int x, int y, char r) {
		Arena[x][y] = r;
	}
	/**
	 * prints the arena 
	 */
	public String toString() {
		String res = "";
		for (int i = 0; i < w; i++) {
			for(int j = 0; j < h; j++) {
				res = res + Arena[i][j];
			}
			res = res + "\n";
		}
		return res;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ConsoleCanvas c = new ConsoleCanvas (10,20);
		c.showIt(4,3, 'D');
		System.out.println(c.toString());
	}

}
