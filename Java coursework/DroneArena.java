/**
 * 
 */
package droneSim;

import java.util.ArrayList;
import java.util.Random;
/**
 * @author Axel
 *
 */
public class DroneArena {
	
	private int xMax, yMax; //size of arena
	ArrayList<Drone> manyDrones; //array of Drones
	Random randomGenerator; //creating random object
	private Drone d;
	
/**
 * constructor
 * @param x
 * @param y
 */
	public DroneArena( int x, int y) {
		xMax = x;
		yMax = y;
		manyDrones = new ArrayList<Drone>();
		randomGenerator = new Random();
	}
	
	/**
	 * getter for size of X
	 */
	public int getXSize() {
		return xMax;
	}
	
	/**
	 * getter for size of Y
	 */
	public int getYSize() {
		return yMax;
	}
	
	/**
	 * @param dNum
	 * adds Drones to arena at a random location, using a for loop 
	 */
	public void addDrone() {
		
		int rx, ry;
		boolean res = false;
		
		do {
			rx = randomGenerator.nextInt(xMax);
			ry = randomGenerator.nextInt(yMax);
			if (getDroneAt(rx, ry) == null) { 
				Drone d = new Drone(rx, ry, Direction.randomDir());
				manyDrones.add(d);
				res = true;
				//System.out.println(d.toString());
			}
		}while(res == false);
	}
	
	/**
	 * returns drone arena size and all drone locations using a for loop
	 */
	public String toString() {
		String res = "";
		for( Drone d : manyDrones) { res += d.toString() + "\n"; }
		return res;
	}
	
	/**
	 * searches through ArrayList of drones and uses the isHere method to check if any drones are at the specified location
	 * @param x
	 * @param y
	 * @return
	 */
	public Drone getDroneAt(int x, int y) {
		for (int d = 0; d<manyDrones.size(); d++) {
			if(manyDrones.get(d).isHere(x, y) == true) { return manyDrones.get(d); }
		}
		return null;
	}
	
	/**
	 * loops through the drone array list, places drone on the canvas
	 * @param c
	 */
	public void showDrones(ConsoleCanvas c) {
		for(Drone d : manyDrones) {
			d.displayDrone(c);
		}
	}
	/**
	 * returns true of false to see if a location is used by a drone or is out of the arena boundries
	 * @param x
	 * @param y
	 * @return
	 */
	public boolean canMoveHere(int x, int y) {
		if (x > xMax | y > yMax) {
			return false; 
		}
		else {
			if(getDroneAt(x, y) == null) {
				return true;
			}
			else return false;
		}
	}
	/**
	 * goes through array list and calls to move each drone 
	 */
	public void moveAllDrones() {
		for(Drone d : manyDrones) {
			d.tryToMove(this);
		}
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		DroneArena a = new DroneArena(20, 10); // create drone arena
		a.addDrone(); //add drone to arena
		System.out.println(a.toString()); // print drone location within arena
	}

}
