/**
 * 
 */
package droneSim;

import java.util.Random;
/**
 * @author Axel
 *
 */
public enum Direction {
	NORTH, EAST, SOUTH, WEST;
	
	public static Direction randomDir() {
		Random generator = new Random();
		return values()[generator.nextInt(values().length)];
	}
	
	public Direction next() {
		return values()[(ordinal() + 1) % values().length];
	}
}
