/**
 * 
 */
package droneSim;

/**
 * @author Axel
 *
 */
public class Drone {

	private int x, y, droneID, dx, dy; //create private variables
	private static int droneCounter = 0; //static unique identifier
	private Direction d; //enum
	
	/**
	 * construct drone at position bx, by
	 * @param bx
	 * @param by
	 */
	public Drone(int bx, int by, Direction di) {
		//Constructor
		x = bx;
		y = by;
		droneID = droneCounter++;
		dx = 1;
		dy = 1;
		d = di;
	}
	
	/**
	 * return drone analysis as String
	 */
	public String toString() {
		return ("drone " + droneID + " at position (" + x + "," + y + "), facing direction: " + d.toString()); //returns drone location and ID
	}
	
	/**
	 * x getter
	 * @return x 
	 */
	public int getX() {
		return x;
	}
	
	/**
	 * y getter
	 * @return y
	 */
	 public int getY() {
	 
		return y;
	}
	
	/**
	 * set values of x and y
	 * @param nx
	 * @param ny
	 */
	public void setXY(int nx, int ny) {
		x = nx;
		y = ny;
	}
	
	/**
	 * boolean method returns if drone is at location given from arguments
	 * @param sx
	 * @param sy
	 * @return
	 */
	public boolean isHere(int sx, int sy) {
		if(sx == x & sy == y) return true;
		else return false;
	}
	
	/**
	 * calls the showIt function to display a drone on the canvas
	 * @param c
	 */
	public void displayDrone(ConsoleCanvas c) {
		c.showIt(x, y, 'D');
	}
	
	/**
	 * switch case to determine which direction to move the drone in
	 * each case checks to see if a drone can be moved to that location
	 * @param a
	 */
	public void tryToMove(DroneArena a) {
		int newX, newY;
		switch (d.toString()) {
		
			case "NORTH":
				newY = y + dy;
				if (a.canMoveHere(x, newY) == true) { 
					setXY(x, newY);
					break; 
				}
				else {
					d = d.next();
					break; 
				}
					
			case "EAST":
				newX = x + dx;
				if (a.canMoveHere(newX, y) == true) {
					setXY(newX, y);
					break;
				}
				else {
					d = d.next();
					break;
				}
				
			case "SOUTH":
				newY = y - dy;
				if (a.canMoveHere(x, newY) == true) { 
					setXY(x, newY);
					break; 
				}
				else {
					d = d.next();
					break; 
				}
			case "WEST":
				newX = x - dx;
				if (a.canMoveHere(newX, y) == true) {
					setXY(newX, y);
					break;
				}
				else {
					d = d.next();
					break;
				}
		}
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {		
		Drone d2 = new Drone(10, 4, Direction.NORTH); //create second drone using int input
		System.out.println(d2.toString()); //print drone location
		DroneArena a = new DroneArena(20,10);
		d2.tryToMove(a);
		System.out.println(d2.toString());
	}

}
