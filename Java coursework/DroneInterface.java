/**
 * 
 */
package droneSim;

import java.util.Scanner;

/**
 * @author Axel
 *
 */
public class DroneInterface {
	
	private Scanner s;
	private DroneArena myArena;
	
	/**
	 * constructor
	 * sets up scanner for input and the arena
	 * main loop allows for user commands
	 */
	public DroneInterface() {
		s = new Scanner(System.in);
		myArena = new DroneArena(10, 10);
		
		char ch = ' ';
		do {
			System.out.println("Enter (A)dd drone, get (I)nformation, (D)isplay canvas, (M)ove Drones, or e(X)it > ");
			ch = s.next().charAt(0);
			s.nextLine();
			switch (ch) {
				case 'A' :
				case 'a' :
							myArena.addDrone();
							break;
				case 'I' :
				case 'i' :
							System.out.print(myArena.toString());
							break;
				case 'x' :  ch = 'X';
							break;
				case 'D':
				case 'd':
							doDisplay();
							break;
				case 'M':
				case 'm':
							myArena.moveAllDrones();
							break;
			}
		}while(ch != 'X');
	}

	void doDisplay() {
		int x = myArena.getXSize();
		int y = myArena.getYSize();
		ConsoleCanvas Arena = new ConsoleCanvas(x, y);
		myArena.showDrones(Arena);
		System.out.println(Arena.toString());
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		DroneInterface r = new DroneInterface();

	}

}
